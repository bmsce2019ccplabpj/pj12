#include<stdio.h>
int size()
{
  int n;
  printf("Enter the size of the array\n");
  scanf("%d",&n);
  return n;
}
void input(int n,int a[n])
{
  int i;
  printf("Enter the elements of array\n");
  for(i=0;i<n;i++)
  scanf("%d",&a[i]);
}
void compute(int n,int a[n],int *s,int *spos,int *l,int *lpos)
{
  int i;
  *s=a[0];
  *spos=0;
  for(i=1;i<n;i++)
{
  if(a[i]<*s)
{
  *s=a[i];
  *spos=i;
}}
    *l=a[0];
    *lpos=0;
    for(i=1;i<n;i++)
    {
  if(a[i]>*l)
{
  *l=a[i];
  *lpos=i;
}
}
}
void output1(int s,int spos,int l,int lpos)
{
  printf("The smallest no.:%d\n",s);
  printf("The position of %d:%d\n",s,spos+1);
  printf("The largest no.:%d\n",l);
  printf("The position of %d:%d\n",l,lpos+1);
}
void interchange(int n,int a[n],int spos,int lpos)
{
  int t;
  t=a[spos];
  a[spos]=a[lpos];
  a[lpos]=t;
}
void output2(int n,int a[n],int spos,int lpos)
{
 printf("After interchanging\n");
 printf("Smallest no %dis in the position %d\n",a[lpos],lpos+1);
 printf("Largest no.  %d is in position %d\n",a[spos],spos+1);
 printf("The elements of array are\n");
 for(int i=0;i<n;i++)
 {
  printf("\n%d",a[i]);
  }
}
int main()
{
  int n,i,s,spos,l,lpos;
  n=size();
  int a[n];
  input(n,a);
  compute(n,a,&s,&spos,&l,&lpos);
  output1(s,spos,l,lpos);
  interchange(n,a,spos,lpos);
  output2(n,a,spos,lpos);
}