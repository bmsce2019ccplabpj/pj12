#include<stdio.h>
#include<math.h>
struct point
{
float x,y;
};
typedef struct point POINT;
POINT input()
{
 POINT p;
 printf("Enter the coordinates of point\n");
 scanf("%f%f",&p.x,&p.y);
 return p;
}
float compute(POINT p1,POINT p2)
{
float dis;
dis=sqrt(pow((p1.x-p2.x),2) + pow((p1.y-p2.y),2));
return dis;
}
void output(float dis)
{
printf("The distance between two points %f",dis);
}
int main()
{
POINT  p1 ,p2;
float distance;
p1=input();
p2=input();
distance=compute(p1,p2);
output(distance);
return 0;
}
