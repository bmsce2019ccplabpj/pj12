#include<stdio.h>
struct numbers
{
    int n, d;
};
typedef struct numbers fract;
int no()
{
    int n;
    printf("Enter the no. of fractions\n");
    scanf("%d",&n);
    return n;
}
fract input(int n, fract f[n])
{
    for(int i=0;i<n;i++) 
    {
    printf("Enter numerator and denominator of fraction %d\n",i+1);
    scanf("%d%d",&f[i].n,&f[i].d);
    }
}
fract add_n_fractions(int n, fract f[n]) 
{   fract sum;
    sum.n=f[0].n;
    sum.d=f[0].d;
    for(int i=1;i<n;i++)
    {
    sum.n=(sum.n * f[i].d)+(sum.d * f[i].n);
    sum.d=sum.d*f[i].d;
    }

	return sum;
}
int gcd(int ns,int ds) 
{
	int t;
    while(ds!=0)
	{
       t = ds; 
       ds=ns%ds; 
       ns= t; 
	}
    return ns;
}
fract reduce(fract sum) 
{   int g;
    g=gcd(sum.n,sum.d); 
    sum.n=sum.n/g;
    sum.d=sum.d/g;
    return sum;
}
void output(fract sum)
{ 
    if(sum.d==1)
    {
        printf("SUM=%d",sum.n);
    }
    else 
    printf("SUM=%d/%d",sum.n,sum.d);
}
int main() 
{
    int n;
    n=no();
    fract f[n], sum;
    input(n,f) ;
    sum=add_n_fractions(n,f);
    sum=reduce(sum);
    output(sum) ;        
 }