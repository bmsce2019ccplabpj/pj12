
    #include<stdio.h>
int input()
{
    int num;
    printf("Enter an integer or 0 to exit\n");
    scanf("%d",&num);
    return num;
}
void compute(int num,int *p,int *n,float *avg)
{
    int sum=0,count=0;
    *p=0,*n=0;
    while(num!=0)
    {
    if(num>0)
    {
    *p=*p+1;
    sum=sum+num;
    count++;
    }
    else
    {
    *n=*n+1;
    }
    printf("Enter an integer\n");
    scanf("%d",&num);
    }
    *avg=(float)sum/count;
    return;
}
void output(int p,int n,float avg)
{
    printf("Total no. of positive integers=%d",p);
    printf("Total no. of negative integers=%d",n);
    printf("Average of positive integers=%.2f",avg);
}
int main()
{
    int num,p,n;
    float avg;
    num=input();
    compute(num,&p,&n,&avg);
    output(p,n,avg);
    return 0;
}
