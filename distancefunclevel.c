#include<stdio.h>
#include<math.h>
float inputx()
{
  float x;
  printf("Enter the x coordinates\n");
  scanf("%f",&x);
  return x;
}
float inputy()
{
  float y;
  printf("Enter the y coordinate\n");
  scanf("%f",&y);
  return y;
}
float compute(float x1,float x2,float y1,float y2)
{
  float dis;
  dis=sqrt(pow(x2-x1,2)+pow(y2-y1,2);
  return dis;
}
void output(float dis)
{
  printf("Distance between two points=%f",dis);
}
int main()
{
  float x1,x2,y1,y2,distance;
  x1=inputx();
  y1=inputy();
  x2=inputx();
  y2=inputy();
  distance=compute(x1,y1,x2,y2);
  output(distance);
  return 0;
}
