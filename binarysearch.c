#include <stdio.h>
int input1()
{
    int n;
    printf("Enter the number of elements to be inserted\n");
    scanf("%d",&n);
    return n;
}
void input2(int n,int a[n],int *ele)
{
    int i;
    printf("Enter %d elements of array\n",n);
    for(i=0;i<n;i++)
{
    scanf("%d",&a[i]);
}
    printf("Enter the element to be searched\n");
    scanf("%d",ele);
}
void check(int n,int a[n],int ele,int *found,int *pos )
{
   int beg=0,end=n-1,mid;
   *found=0;
   while(beg<=end)
   {
    mid=(beg+end)/2;
    if(a[mid]==ele)
    {
     *pos=mid;
     *found=1;
     break;
    }
    else if(a[mid]>ele)
    {
     beg=mid+1;
    }
    else
    {
     end=mid-1;
    }   
   }  
}
void output(int found, int pos ,int ele)
{
    if(found==1)
{
    printf("The ele %d is found in the position=%d",ele,pos);
}
    else 
{
    printf("the element %d is not found in the array",ele);
}
}
int main()
{
    int n,pos,ele,found;
    n=input1();
    int a[n];
    input2(n,a,&ele);
    check(n,a,ele,&found,&pos);
    output(found,pos,ele);
    return 0;
}