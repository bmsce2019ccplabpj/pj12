#include<stdio.h>
struct date
{
    int day;
    char month[100];
    int year;
};
struct employee
{
    int id;
    char name[40];
    float sal;
    struct date DOJ[100];
};
void no_of_employees( int *n)
{
    printf("Enter the number of employees\n");
    scanf("%d",n);
}
void input(int n,struct employee E[n])
{
        for(int i=0;i<n;i++)
        {
         printf("ENTER THE EMPLOYEE DETAILS\n");
         printf("Enter the id of %d employee:\n",i+1);
         scanf("%d",&E[i].id);
         printf("Enter the name of %d employee:\n",i+1);
         scanf("%s",&E[i].name);
         printf("Enter the salary of %d employee:\n",i+1);
         scanf("%f",&E[i].sal);
         printf("Enter the date of join of %d employee:\n",i+1);
         scanf("%s",&E[i].DOJ);
        }
}
void output(int n,struct employee E[n])
{
    for(int i=0;i<n;i++)
    {
        printf("DETAILS OF THE EMPLOYEE %d:\n",i+1);
        printf("Employee id-%d\n",E[i].id);
        printf("Employee name-%s\n",E[i].name);
        printf("Employee salary-%.2f\n",E[i].sal);
        printf("Date of join of the employee-%s\n",E[i].DOJ);
    }
}
int main()
{
    int n;
    no_of_employees(&n);
    struct employee E[n];
    input(n,E);
    output(n,E);
    return 0;
}